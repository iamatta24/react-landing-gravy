import axios from "axios";
import { API_URL } from "../config/api";

export function postReferService(data) {
  return axios.post(API_URL, data);
}
