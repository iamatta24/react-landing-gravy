import axios from "axios";
import { API_URL } from "../config/api";

export function postGiftService(data) {
  return axios.post(API_URL, data);
}
