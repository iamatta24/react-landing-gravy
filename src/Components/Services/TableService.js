import axios from "axios";
import { API_URL } from "../config/api";

export function getTableService() {
  return axios.get(API_URL);
}

export function postTableTextService(data) {
  return axios.get(
    "https://jsonplaceholder.typicode.com/comments/" + `?postId=${data}`
  );
}

export function getTableSortService(data) {
  return axios.get(
    "https://jsonplaceholder.typicode.com/comments/" + `?postId=${data}`
  );
}
