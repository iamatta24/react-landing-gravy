import { combineReducers } from "redux";
import userReducer from "./signup/userReducer";
import tableReducer from "./Table/TableReducer";
import giftReducer from "./GiftCardRedeem/GiftReducer";
import signinReducer from "./SignIn/SignInReducer";
import referReducer from "./ReferFriend/ReferReducer";

export default combineReducers({
  table: tableReducer,
  user: userReducer,
  gift: giftReducer,
  signin: signinReducer,
  refer: referReducer,
});
