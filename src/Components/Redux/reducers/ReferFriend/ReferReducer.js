import { POST_REFER } from "../../actions/types";

const INITIAL_STATE = {
  data: [],
  loading: false,
};

const referReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case POST_REFER:
      return { ...state, data: state.data.concat(action.payload) };

    default:
      return state;
  }
};
export default referReducer;
