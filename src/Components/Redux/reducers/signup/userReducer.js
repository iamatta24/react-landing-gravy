import {POST_USER } from "../../actions/types";

  const INITIAL_STATE = {
    data: [],
    loading: false,
  };

const userReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      
      case POST_USER:
        return { ...state, data: state.data.concat(action.payload) };
     
     
      default:
        return state;
    }
  };
  export default userReducer;
  