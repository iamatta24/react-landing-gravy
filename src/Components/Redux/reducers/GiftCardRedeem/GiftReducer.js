import { POST_GIFT } from "../../actions/types";

const INITIAL_STATE = {
  data: [],
  loading: false,
};

const giftReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case POST_GIFT:
      return { ...state, data: state.data.concat(action.payload) };

    default:
      return state;
  }
};
export default giftReducer;
