import {
  GET_TABLE,
  POST_TABLE_TEXT,
  GET_TABLE_SORT,
} from "../../actions/types";

const INITIAL_STATE = {
  data: [],
  //   loading: false,
};

const tableReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_TABLE:
      return { ...state, data: state.data.concat(action.payload) };
    case POST_TABLE_TEXT:
      return { ...state, data: state.data.concat(action.payload) };
    case GET_TABLE_SORT:
      return { ...state, data: state.data.concat(action.payload) };
    default:
      return state;
  }
};
export default tableReducer;
