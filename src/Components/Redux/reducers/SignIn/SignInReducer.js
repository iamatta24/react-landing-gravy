import { POST_SIGNIN } from "../../actions/types";

const INITIAL_STATE = {
  data: [],
  loading: false,
};

const signinReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case POST_SIGNIN:
      return { ...state, data: state.data.concat(action.payload) };

    default:
      return state;
  }
};
export default signinReducer;
