import { postSignInService } from "../../../Services/SignInService";
import { POST_SIGNIN } from "../types";

export const postSignIn = (user) => async (dispatch) => {
  try {
    const response = await postSignInService(user);
    dispatch({
      type: POST_SIGNIN,
      payload: response.data,
    });
    // history.push("/posts");
  } catch (error) {}
};
