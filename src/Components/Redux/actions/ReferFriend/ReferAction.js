import { postReferService } from "../../../Services/ReferService";
import { POST_REFER } from "../types";

export const postRefer = (data) => async (dispatch) => {
  try {
    const response = await postReferService(data);
    dispatch({
      type: POST_REFER,
      payload: response.data,
    });
    console.log("Hello");
    // history.push("/posts");
  } catch (error) {}
};
