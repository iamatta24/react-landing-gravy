// SignUp Page
export const POST_USER = "POST_USER";

// SignIn Page
export const POST_SIGNIN = "POST_SIGNIN";

// Dashboard Page
export const GET_TABLE = "GET_TABLE";
export const POST_TABLE_TEXT = "POST_TABLE_TEXT";
export const GET_TABLE_SORT = "GET_TABLE_SORT";

// Gift Card Redeem Page
export const POST_GIFT = "POST_GIFT";

// Refer Friend Page
export const POST_REFER = "POST_REFER";
