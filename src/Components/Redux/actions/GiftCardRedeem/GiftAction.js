import { postGiftService } from "../../../Services/GiftService";
import { POST_GIFT } from "../types";

export const postGift = (data) => async (dispatch) => {
  try {
    const response = await postGiftService(data);
    dispatch({
      type: POST_GIFT,
      payload: response.data,
    });
    console.log("Hello");
    // history.push("/posts");
  } catch (error) {}
};
