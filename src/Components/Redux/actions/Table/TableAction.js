import { GET_TABLE, POST_TABLE_TEXT, GET_TABLE_SORT } from "./../types";
import {
  getTableService,
  postTableTextService,
  getTableSortService,
} from "../../../Services/TableService";

export const getTable = () => async (dispatch) => {
  try {
    const response = await getTableService();
    dispatch({
      type: GET_TABLE,
      payload: response.data,
    });
  } catch (error) {}
};

export const postTableText = (filterValue) => async (dispatch) => {
  try {
    const response = await postTableTextService(filterValue);
    dispatch({
      type: POST_TABLE_TEXT,
      payload: response.data,
    });
  } catch (error) {}
};

export const getTableSort = (data) => async (dispatch) => {
  try {
    const response = await getTableSortService(data);
    dispatch({
      type: GET_TABLE_SORT,
      payload: response.data,
    });
  } catch (error) {}
};
