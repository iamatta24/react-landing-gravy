import { postUserService } from "../../../Services/UserService";
import { POST_USER } from "../types";

export const postUser = (user) => async (dispatch) => {
  try {
    const response = await postUserService(user);
    dispatch({
      type: POST_USER,
      payload: response.data,
    });
    // history.push("/posts");
  } catch (error) {}
};
