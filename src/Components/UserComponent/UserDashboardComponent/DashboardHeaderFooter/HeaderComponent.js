import React from "react";
import {
  Row,
  Col,
  Container,
  Button,
  Table,
  Nav,
  Navbar,
  NavDropdown,
  Form,
  FormControl,
} from "react-bootstrap";
import "./HeaderStyle.css";
import GravyIcon from "../../../../Asserts/GravyIcon.png";
import Time from "../../../../Asserts/Time.png";
import PendingSuccess from "./../../../Common/PendingSuccess";

const headerComponent = () => {
  return (
    <>
      <Navbar collapseOnSelect expand="lg" className="header1 font">
        <Navbar.Brand href="#">
          <img src={GravyIcon} className="my-3 mx-4" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mx-auto"></Nav>
          <Nav className="my-4 mr-1">
            <Nav.Link
              href="/dashboard"
              className="mx-4 mt-1"
              style={{ color: "black" }}
            >
              <h5>
                <b>Dashboard</b>
              </h5>
            </Nav.Link>
            <Nav.Link
              href="/CustomerGiftCardRedeemRNA"
              className="mx-4 mt-1"
              style={{ color: "black" }}
            >
              <h5>
                <b>Redeem Gift Cards</b>
              </h5>
            </Nav.Link>
            <Nav.Link
              href="/referFriend"
              style={{ color: "black" }}
              className="mx-4 mt-1"
            >
              <h5>
                <b>Refer A Friend</b>
              </h5>
            </Nav.Link>
            <Nav.Link href="/profileComponent" className="mx-4">
              <Row className="header1">
                {/* <Col xl={3}>
                  <PendingSuccess type="pending" size="40px" />
                </Col>
                <Col xl={9}>
                  <h5 className="accounts">
                    <b style={{ color: "black" }}>Your Accounts</b>
                  </h5>
                  <small style={{ fontSize: "11px", color: "#F47458" }}>
                    Status: Pending
                  </small>
                </Col> */}
                <Col>
                  <div className="d-flex">
                    <PendingSuccess type="pending" width="35px" height="35px" />
                    <div className="ml-3">
                      <h5 className="accounts">
                        <b style={{ color: "black" }}>Your Accounts</b>
                      </h5>
                      <small style={{ fontSize: "11px", color: "#F47458" }}>
                        Status: Pending
                      </small>
                    </div>
                  </div>
                </Col>
              </Row>
            </Nav.Link>
            <Nav.Link href="#" className="pt-0">
              <Button className="button shadow-none">
                <b>Install Extension</b>
              </Button>
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default headerComponent;
