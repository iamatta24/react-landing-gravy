import React from "react";
import {
  Row,
  Col,
  Dropdown,
  Button,
  Table,
  Form,
  Container,
} from "react-bootstrap";

import { connect } from "react-redux";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronDown,
  faFilter,
  faSearch,
  faSort,
} from "@fortawesome/free-solid-svg-icons";

import { postTableText } from "./../../../Redux/actions/Table/TableAction";

const ColumnFilterText = ({ column, postTableText }) => {
  const { filterValue, setFilter } = column;

  const handleSubmit = (e) => {
    e.preventDefault();
    postTableText(filterValue);
  };

  const handleReset = () => {
    setFilter([]);
  };

  return (
    <Dropdown>
      <Dropdown.Toggle id="dropdown-basic">
        <FontAwesomeIcon icon={faSearch} />
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Container id="checkBox">
          <Row>
            <Col>
              <Form onSubmit={handleSubmit}>
                <Form.Group controlId="search">
                  <Form.Control
                    type="text"
                    name=""
                    placeholder="Search Company"
                    value={filterValue || ""}
                    onChange={(e) => setFilter(e.target.value)}
                  />
                </Form.Group>
                <Dropdown.Divider />
                <Button variant="warning" type="reset" onClick={handleReset}>
                  Reset
                </Button>
                <Button variant="warning" type="submit" className="mx-1">
                  Ok
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default connect(null, { postTableText })(ColumnFilterText);
