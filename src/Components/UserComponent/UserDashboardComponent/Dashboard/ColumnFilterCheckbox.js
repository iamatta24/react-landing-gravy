import React from "react";
import {
  Row,
  Col,
  Dropdown,
  Button,
  Table,
  Form,
  Container,
} from "react-bootstrap";
import "./TableStyle.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronDown,
  faFilter,
  faSearch,
  faSort,
} from "@fortawesome/free-solid-svg-icons";

export const ColumnFilterCheckbox = ({ column }) => {
  const { filterValue, setFilter } = column;

  return (
    <Dropdown>
      <Dropdown.Toggle id="dropdown-basic">
        <FontAwesomeIcon icon={faFilter} className="img-fluid" />
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Container id="checkBox">
          <Row>
            <Col>
              <Form>
                <Form.Group controlId="referral">
                  <Form.Check
                    type="checkbox"
                    label="Referral"
                    value={filterValue || ""}
                    onChange={(e) => setFilter(e.target.value)}
                  />
                </Form.Group>
                <Form.Group controlId="websiteView">
                  <Form.Check type="checkbox" label="Website View" />
                </Form.Group>
                <Form.Group controlId="advertView">
                  <Form.Check type="checkbox" label="Advert View" />
                </Form.Group>
                <Dropdown.Divider />
                <Button variant="warning" type="submit">
                  Reset
                </Button>
                <Button variant="warning" type="submit" className="mx-1">
                  Ok
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
      </Dropdown.Menu>
    </Dropdown>
  );
};
