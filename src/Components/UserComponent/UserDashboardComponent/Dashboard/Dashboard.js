import React from "react";
import { Row, Col, Container, Button } from "react-bootstrap";
import happyGirl from "../../../../Asserts/HappyGirl.png";
import Line from "../../../../Asserts/Line.png";
import Time2 from "../../../../Asserts/Time2.png";
import Check from "../../../../Asserts/Check.png";
import Phone from "../../../../Asserts/Phone.png";

import HeaderComponent from "./../../UserDashboardComponent/DashboardHeaderFooter/HeaderComponent";
import FooterComponent from "./../DashboardHeaderFooter/FooterComponent";
import TableComponent from "./TableComponent";

const dashboard = () => {
  return (
    <>
      <HeaderComponent />
      <Container fluid className="font">
        <Row className="header1 pt-5">
          <Col xl={4} xl={{ offset: 1 }}>
            <img className="img-fluid" src={happyGirl} />
          </Col>
          <Col xl={8}>
            <Row>
              <Col xl={5} lg={5} md={5} sm={5} xs={5}>
                <h1 className="MaxText ml-5">
                  Welcome back, <b>Max</b>
                </h1>
                <h4 className="CoinSummary ml-5">Here’s your coin summary</h4>
              </Col>
              <Col xl={1} lg={1} md={1} sm={1} xs={1}>
                <img src={Line} />
              </Col>
              <Col xl={6} lg={5} md={6} sm={6} xs={6}>
                <Row>
                  <Col xl={2} lg={2} md={2} sm={2} xs={2}>
                    <img src={Time2} className="img-fluid" />
                  </Col>
                  <Col xl={9} lg={9} md={9}>
                    <h5>
                      To start collecting coins and to redeem gift cards verify
                      your account. &nbsp;
                      <a href="#" className="p1 text-decoration-none">
                        Get Verified
                      </a>
                    </h5>
                  </Col>
                  <Col xl={1} lg={1} md={1}></Col>
                </Row>
              </Col>
            </Row>

            {/* <Col xl={6} class="d-flex " style={{ backgroundColor: "#FCF9F2" }}> 
              <Row>
              <div class="d-flex ">
              <Col xl={1} className=" pr-5" >
                <img src={Time2} />
                </Col>

                <Col xl={11}>
                <h1>
                  Coins pending:<b>2,400</b>
                </h1>
                </Col>
                 </div>
                </Row> 
              </Col>

              <Col xl={6} style={{ backgroundColor: "#FCF9F2" }}>
              <Row>
              <div class="d-flex px-4" >
              <Col xl={1} className=" pr-5">
              <img src={Check} />
                </Col>

                <Col xl={11}>
                <h1>
                  Total coins: <b>400</b>
                </h1>
                </Col>
                 </div>
                </Row> 
              </Col> */}
            {/* <Row
              className="mt-4 mx-5 py-5 px-5"
              style={{ backgroundColor: "#FCF9F2" }}
            >
              <Col
                xl={1}
                lg={1}
                md={1}
                style={{ backgroundColor: "#FCF9F2" }}
                className="py-5 how-img"
              >
                <img src={Time2} className="img-fluid" />
              </Col>
              <Col
                xl={6}
                lg={6}
                md={6}
                style={{ backgroundColor: "#FCF9F2" }}
                className="py-5"
              >
                <h1>
                  Coins pending: <b>2,400</b>
                </h1>
              </Col>
              <Col
                xl={1}
                lg={1}
                md={1}
                style={{ backgroundColor: "#FCF9F2" }}
                className="py-5 how-img"
              >
                <img src={Check} className="img-fluid" />
              </Col>
              <Col
                xl={4}
                lg={4}
                md={4}
                style={{ backgroundColor: "#FCF9F2" }}
                className="py-5"
              >
                <h1>
                  Total coins: <b>400</b>
                </h1>
              </Col>
            </Row> */}

            <Row className="my-5 mx-4 py-5 px-4 pr-5">
              <Col
                xl={6}
                lg={6}
                md={6}
                sm={12}
                xs={12}
                style={{ backgroundColor: "#FCF9F2" }}
                className="py-5 d-flex"
              >
                <img src={Time2} className="img-fluid pr-5" />
                <h1 className="cointext">
                  Coins pending: <b>60</b>
                </h1>
              </Col>
              <Col
                xl={6}
                lg={6}
                md={6}
                sm={12}
                xs={12}
                style={{ backgroundColor: "#FCF9F2" }}
                className="py-5 d-flex"
              >
                <img src={Check} className="img-fluid pr-5 " />

                <h1 className="cointext">
                  Total coins: <b>40</b>
                </h1>
              </Col>
            </Row>
          </Col>
        </Row>

        <Container fluid>
          <TableComponent />
        </Container>

        <Container fluid>
          <Row className=" px-5">
            <Col xl={{ span: 12}} className="header1 px-5">
              <Row>
                <Col xl={{ span: 5, offset: 1 }} lg={{ span: 5, offset: 1 }} md={{ span: 5, offset: 1 }} sm={{ span: 11, offset: 1 }} xs={{ span: 11, offset: 1 }}>
                  {/* <p className="text-center pt-5 mt-5 p1">
                  500 coins for you and a friend
                </p>
                <h3 className="text-center">
                  Earn coins faster with Gravy’s referral programme
                </h3>
                <p className="text-center pt-3 text-muted">
                  Share the love and receive a £10 gift card for you and a
                  friend.
                </p>
                <div className="text-center">
                  <Button className="button">Invite A Friend</Button>
                </div> */}
                   <Row>
                    <Col xl={{ span: 8 }} lg={{ span: 12 }} md={{ span: 8 }} sm={{ span: 6 }} xm={{ span: 6 }}> 
                      <p className=" pt-5 mt-5 p1 InviteFriend">
                        500 coins for you and a friend
                      </p>
                      <h3 className="Referal">
                        <b>Earn coins faster with Gravy’s referral programme</b>
                      </h3>
                      <p className=" pt-2 text-muted">
                        Share the love and receive a £10 gift card for you and a
                        friend.
                      </p>
                      <Button className=" border-0 button mt-3 mb-5">Invite A Friend</Button>
                    </Col>
                  </Row>
                </Col> 
                <Col xl={{ span: 6 }} lg={{ span: 6 }} md={{ span: 6 }} sm={{ span: 12 }} xs={{ span: 12 }}>
                  <img className="img-fluid" src={Phone} />
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
        <Row>
          <FooterComponent />
        </Row>
      </Container>
    </>
  );
};

export default dashboard;
