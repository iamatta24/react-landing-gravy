import React, { useMemo, useState, useEffect } from "react";
import {
  Row,
  Col,
  Dropdown,
  Button,
  Table,
  Form,
  Modal,
  Container,
} from "react-bootstrap";
import axios from "axios";
import { connect } from "react-redux";

import {
  getTable,
  getTableSort,
} from "./../../../Redux/actions/Table/TableAction";

import { useTable, useSortBy, useFilters, usePagination } from "react-table";
import { ColumnFilterCheckbox } from "./ColumnFilterCheckbox";
import ColumnFilterText from "./ColumnFilterText";

import checkSmall from "../../../../Asserts/CheckSmall.png";
import Vector from "../../../../Asserts/Vector.svg";
import Vector2 from "../../../../Asserts/Vector2.svg";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {
  faChevronDown,
  faFilter,
  faSearch,
  faSort,
} from "@fortawesome/free-solid-svg-icons";


import "./TableStyle.css";

// const COLUMNS = [
//   {
//     Header: () => <div className="text-center">Record Type</div>,
//     accessor: "record_type",
//     Filter: ({ column }) => {
//       const { filterValue, setFilter } = column;

//       return (
//         <Dropdown>
//           <Dropdown.Toggle id="dropdown-basic">
//             <FontAwesomeIcon icon={faFilter} />
//           </Dropdown.Toggle>

//           <Dropdown.Menu>
//             <Container id="checkBox">
//               <Row>
//                 <Col>
//                   <Form>
//                     <Form.Group controlId="referral">
//                       <Form.Check
//                         type="checkbox"
//                         label="Referral"
//                         value={filterValue || ""}
//                         onChange={(e) => setFilter(e.target.value)}
//                       />
//                     </Form.Group>
//                     <Form.Group controlId="websiteView">
//                       <Form.Check type="checkbox" label="Website View" />
//                     </Form.Group>
//                     <Form.Group controlId="advertView">
//                       <Form.Check type="checkbox" label="Advert View" />
//                     </Form.Group>
//                     <Dropdown.Divider />
//                     <Button variant="warning" type="submit">
//                       Reset
//                     </Button>
//                     <Button variant="warning" type="submit" className="mx-1">
//                       Ok
//                     </Button>
//                   </Form>
//                 </Col>
//               </Row>
//             </Container>
//           </Dropdown.Menu>
//         </Dropdown>
//       );
//     },
//     disableSortBy: true,
//   },
//   {
//     Header: () => <span>Company Name</span>,
//     accessor: "company_name",
//     Filter: ({ column }) => {
//       const { filterValue, setFilter } = column;

//       return (
//         <Dropdown>
//           <Dropdown.Toggle id="dropdown-basic">
//             <FontAwesomeIcon icon={faSearch} />
//           </Dropdown.Toggle>

//           <Dropdown.Menu>
//             <Container id="checkBox">
//               <Row>
//                 <Col>
//                   <Form>
//                     <Form.Group controlId="search">
//                       <Form.Control
//                         type="text"
//                         placeholder="Search Company"
//                         value={filterValue || ""}
//                         onChange={(e) => setFilter(e.target.value)}
//                       />
//                     </Form.Group>
//                     <Dropdown.Divider />
//                     <Button variant="warning" type="submit">
//                       Reset
//                     </Button>
//                     <Button variant="warning" type="submit" className="mx-1">
//                       Ok
//                     </Button>
//                   </Form>
//                 </Col>
//               </Row>
//             </Container>
//           </Dropdown.Menu>
//         </Dropdown>
//       );
//     },

//     disableSortBy: true,
//   },
//   {
//     Header: () => (
//       <span>
//         {" "}
//         Coins earnt &ensp; <FontAwesomeIcon icon={faSort} />
//       </span>
//     ),
//     accessor: "coins_earnt",
//     Filter: ColumnFilter,
//     disableFilters: true,
//   },
//   {
//     Header: () => (
//       <span>
//         Date &ensp; <FontAwesomeIcon icon={faSort} />
//       </span>
//     ),
//     accessor: "date",
//     Filter: ColumnFilter,
//     disableFilters: true,
//   },
//   {
//     Header: "Status",
//     accessor: "status",
//     Cell: ({ cell: { value } }) => <img src={checkSmall} />,
//     Filter: ColumnFilter,
//     disableSortBy: true,
//     disableFilters: true,
//   },
//   {
//     Header: "Action",
//     accessor: "action",
//     Cell: ({ cell: { value } }) => <Button className="button">View</Button>,
//     Filter: ColumnFilter,
//     disableSortBy: true,
//     disableFilters: true,
//   },
// ];

const ViewModal = (props) => {
  // const defaultColumn = React.useMemo(
  //   () => ({
  //     Filter: ColumnFilter,
  //   }),
  //   []
  // );
  const [show, setShow] = useState(false);

  const { getTable, data, getTableSort } = props;
  const handleSort = (data) => {
    getTableSort(data);
  };
  const COLUMNS = [
    {
        Header: () => <span>Company</span>,
        accessor: "postId",
        Filter: ColumnFilterText,
        disableSortBy: true,
      },
      {
        Header: () => {
          return (
            <span
              onClick={() => {
                handleSort("time");
              }}
            >
              Time &ensp; <FontAwesomeIcon icon={faSort} />
            </span>
          );
        },
        accessor: "id",
        Filter: ColumnFilterCheckbox,
        disableFilters: true,
      },
      {
        Header: () => <span>URL</span>,
        accessor: "name",
        Filter: ColumnFilterText,
        disableSortBy: true,
      },
      {
        Header: () => (
          <span
            onClick={() => {
              handleSort("device");
            }}
          >
            Device &ensp; <FontAwesomeIcon icon={faSort} />
          </span>
        ),
        accessor: "device",
        Filter: ColumnFilterCheckbox,
        disableFilters: true,
      },
    {
      Header: () => <div className="filter">Browser</div>,
      accessor: "body",
      Filter: ColumnFilterCheckbox,
      disableSortBy: true,
    },
   
   
    {
      Header: () => (
        <span
          onClick={() => {
            handleSort("date");
          }}
        >
          Date &ensp; <FontAwesomeIcon icon={faSort} />
        </span>
      ),
      accessor: "email",
      Filter: ColumnFilterCheckbox,
      disableFilters: true,
    },
    // {
    //   Header: "Status",
    //   accessor: "status",
    //   Cell: ({ cell: { value } }) => <img src={checkSmall} />,
    //   Filter: ColumnFilterCheckbox,
    //   disableSortBy: true,
    //   disableFilters: true,
    // },
    // {
    //   Header: "Action",
    //   accessor: "action",
    //   Cell: ({ cell: { value } }) => <Button className="button" onClick={() => setShow(true)}>View</Button>,
    //   Filter: ColumnFilterCheckbox,
    //   disableSortBy: true,
    //   disableFilters: true,
    // },
  ];
  //const [data, setData] = useState([]);

  useEffect(() => {
    getTable();
    //console.log(data);
    // (async () => {
    //   const result = await axios.get(
    //     "https://jsonplaceholder.typicode.com/comments"
    //   );
    //   setData(result.data);
    // })();
  }, []);
  const columns = useMemo(() => COLUMNS, []);
  // const data = useMemo(() => MOCK_DATA, []);

  const tableInstance = useTable(
    {
      columns,
      data,
      // defaultColumn,
    },
    useFilters,
    useSortBy,
    usePagination
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    canNextPage,
    canPreviousPage,
    pageOption,
    setPageSize,
    state,
    prepareRow,
  } = tableInstance;

  const { pageIndex, pageSize } = state;

  return (
    <>
      <Row >
        <Col  xl={{ span: 12}} className="p-5  ">
          <Table
            borderless
            varient
            responsive
            hover
            size="xl"
            className="table"
            {...getTableProps()}
          >
            <thead size="xl">
              {headerGroups.map((headerGroup) => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column) => (
                    <th
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                    >
                      {column.render("Header")}
                      <span>
                        {column.isSorted
                          ? column.isSortedDesc
                            ? console.log("Sorting Descending")
                            : console.log("Sorting Ascending")
                          : ""}
                      </span>
                      <div>
                        {column.canFilter ? column.render("Filter") : null}
                      </div>
                    </th>
                  ))}
                </tr>
              ))}
            </thead>
            <tbody {...getTableBodyProps()}>
              {page.map((row) => {
                prepareRow(row);
                return (
                  <tr {...row.getRowProps()}>
                    {row.cells.map((cell) => {
                      return (
                        <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                      );
                    })}
                  </tr>
                );
              })}
            </tbody>
          </Table>
          </Col>
          {/* </div> */}
      </Row>
      <Row>
        <div className="pb-5 pl-5 offset-xl-7 col-xl-5 offset-lg-7 col-lg-5  offset-md-4 col-md-8 offset-sm-4 col-sm-8 offset-xm-4 col-xm-8">
          <Row>
            <div className="pl-5 offset-xl-1 col-xl-2  offset-lg-1 col-lg-2  col-md-2 col-sm-3 col-xs-3">
              <Button
                onClick={() => previousPage()}
                disabled={!canPreviousPage}
                className="buttonPage border-0"
              >
                <img src={Vector} />
              </Button>
            </div>
            <div className=" offset-xl-1 col-xl-2 col-lg-1 col-md-2 col-sm-2 col-xs-2">
              <Button className="buttonPage">{pageIndex + 1}</Button>
            </div>
            <div className=" col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2">
              <Button
                onClick={() => nextPage()}
                disabled={!canNextPage}
                className="buttonPage border-0"
              >
                <img src={Vector2} />
              </Button>
            </div>
            <div className="   col-xl-3 col-md-3 col-sm-3 col-xs-3">
              <Form>
                <Form.Group controlId="exampleForm.ControlSelect1">
                  <Form.Control
                    as="select"
                    placeholder="Re-type password"
                    value={pageSize}
                    onChange={(e) => setPageSize(Number(e.target.value))}
                    className="buttonPage"
                  >
                    {[10, 20, 30, 40, 50].map((pageSize) => (
                      <option key={pageSize} value={pageSize}>
                        {pageSize}/page
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
              </Form>
            </div>
          </Row>
        </div>

        {/* 
        <Col md={7}></Col>
        <Col md={1}>
          <Button
            onClick={() => previousPage()}
            disabled={!canPreviousPage}
            className="buttonPage border-0"
          >
            Previous
          </Button>
        </Col>
        <Col md={1}>
          <strong className="buttonPage">{pageIndex + 1}</strong>
        </Col>
        <Col md={1}>
          <Button
            onClick={() => nextPage()}
            disabled={!canNextPage}
            className="buttonPage border-0"
          >
            Next
          </Button>
        </Col>
        <Col md={1}>
          <Form>
            <Form.Group controlId="exampleForm.ControlSelect1">
              <Form.Control
                as="select"
                value={pageSize}
                onChange={(e) => setPageSize(Number(e.target.value))}
                className="buttonPage"
              >
                {[10, 20, 30, 40, 50].map((pageSize) => (
                  <option key={pageSize} value={pageSize}>
                    {pageSize}/page
                  </option>
                ))}
              </Form.Control>
            </Form.Group>
          </Form>
        </Col> */}
      </Row>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.table.data,
    // loading: state.category.loading,
  };
};

export default connect(mapStateToProps, { getTable, getTableSort })(ViewModal);
