import "../../../Validations/formik.css";
import React from "react";
import { Button, Form } from "react-bootstrap";
import { withFormik } from "formik";
import { TextInput } from "../../../Validations/TextField";
import { formValidtorProfileEmail } from "../../../Validations/ProfileValidation";
import { Row, Col } from "react-bootstrap";
import * as Yup from "yup";

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape(formValidtorProfileEmail),
  mapPropsToValues: ({ user }) => ({
    ...user,
  }),
  handleSubmit: (payload, { setSubmitting }) => {
    alert(JSON.stringify(payload));
    setSubmitting(false);
  },
  displayName: "MyForm",
});
const MyForm = (props) => {
  const {
    values,
    touched,
    errors,
    dirty,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    isSubmitting,
  } = props;
  return (
    // <Row>
    //   <Col xl={6} lg={12} md={12} sm={12} xs={12}>
    <Form onSubmit={handleSubmit} className="card-text">
      <TextInput
        id="email"
        type="email"
        placeholder="Email"
        error={touched.email && errors.email}
        value={values.email}
        onChange={handleChange}
        onBlur={handleBlur}
        className="formField"
      />
      <Button
        type="submit"
        disabled={isSubmitting}
        variant="warning"
        className="border-0 customButton px-4"
      >
        <b>Save</b>
      </Button>
    </Form>
    //   </Col>
    // </Row>
  );
};
export const ProfileComponentEmailForm = formikEnhancer(MyForm);
