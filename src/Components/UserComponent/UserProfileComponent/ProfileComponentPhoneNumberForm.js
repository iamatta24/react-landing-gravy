import "../../../Validations/formik.css";
import React from "react";
import { Button, Form } from "react-bootstrap";
import { withFormik } from "formik";
import { TextInput } from "../../../Validations/TextField";
import { formValidtorProfilePhoneNumber } from "../../../Validations/ProfileValidation";
import * as Yup from "yup";

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape(formValidtorProfilePhoneNumber),
  mapPropsToValues: ({ user }) => ({
    ...user,
  }),
  handleSubmit: (payload, { setSubmitting }) => {
    alert(JSON.stringify(payload));
    setSubmitting(false);
  },
  displayName: "MyForm",
});
const MyForm = (props) => {
  const {
    values,
    touched,
    errors,
    dirty,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    isSubmitting,
  } = props;
  return (
    <Form onSubmit={handleSubmit} className="card-text">
      <TextInput
        id="phoneNumber"
        type="number"
        placeholder="Phone"
        error={touched.phoneNumber && errors.phoneNumber}
        value={values.phoneNumber}
        onChange={handleChange}
        onBlur={handleBlur}
        className="formField"
      />
      <Button
        type="submit"
        disabled={isSubmitting}
        variant="warning"
        className="border-0 customButton px-4"
      >
        <b>Save</b>
      </Button>
    </Form>
  );
};
export const ProfileComponentPhoneNumberForm = formikEnhancer(MyForm);
