import "../../../Validations/formik.css";
import React from "react";
import { Button, Form, Card } from "react-bootstrap";
import { withFormik } from "formik";
import { TextInput } from "../../../Validations/TextField";
import { formValidtorProfilePassword } from "../../../Validations/ProfileValidation";
import * as Yup from "yup";

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape(formValidtorProfilePassword),
  mapPropsToValues: ({ user }) => ({
    ...user,
  }),
  handleSubmit: (payload, { setSubmitting }) => {
    alert(JSON.stringify(payload));
    setSubmitting(false);
  },
  displayName: "MyForm",
});
const MyForm = (props) => {
  const {
    values,
    touched,
    errors,
    dirty,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    isSubmitting,
  } = props;
  return (
    <>
      <div>
        <Form onSubmit={handleSubmit} className="card-text">
          <TextInput
            id="oldPassword"
            type="password"
            placeholder="Old Password"
            error={touched.oldPassword && errors.oldPassword}
            value={values.oldPassword}
            onChange={handleChange}
            onBlur={handleBlur}
            className="formField"
          />
          <TextInput
            id="newPassword"
            type="password"
            placeholder="New Password"
            error={touched.newPassword && errors.newPassword}
            value={values.newPassword}
            onChange={handleChange}
            onBlur={handleBlur}
            className="formField"
          />

          <Button
            type="submit"
            disabled={isSubmitting}
            className="border-0 customButton"
          >
            <b>Change Password</b>
          </Button>
        </Form>
      </div>
    </>
  );
};
export const ProfileComponentPasswordForm = formikEnhancer(MyForm);
