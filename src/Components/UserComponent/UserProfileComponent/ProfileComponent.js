import React, { useState } from "react";
import {
  Row,
  Col,
  Container,
  Button,
  Card,
  Modal,
  Nav,
  NavDropdown,
  Navbar,
  Form,
  FormControl,
} from "react-bootstrap";
import crossIcon from "../../../Asserts/crossIcon.svg";

import Group from "../../../Asserts/Group.png";
import Rectangle1 from "../../../Asserts/Rectangle1.png";
import Rectangle2 from "../../../Asserts/Rectangle2.png";
import Rectangle3 from "../../../Asserts/Rectangle3.png";
import Timer from "../../../Asserts/Timer.png";
import Tick from "../../../Asserts/Tick.png";
import TimeCardRed from "../../../Asserts/TimeCardRed.png";
import TimeCardGreen from "../../../Asserts/TimeCardGreen.png";

import DashboardHeader from "../UserDashboardComponent/DashboardHeaderFooter/HeaderComponent";
import DashboardFooter from "../UserDashboardComponent/DashboardHeaderFooter/FooterComponent";
import "./Profile.css";
import { ProfileComponentAboutForm } from "./ProfileComponentAboutForm";
import { ProfileComponentEmailForm } from "./ProfileComponentEmailForm";
import { ProfileComponentPhoneNumberForm } from "./ProfileComponentPhoneNumberForm";
import { ProfileComponentPasswordForm } from "./ProfileComponentPasswordForm";
import { Link } from "react-router-dom";
import LogoutComponent from "../UserLogoutComponent/LogoutComponent";

const ProfileComponent = () => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  return (
    <>
      <DashboardHeader />

      <Container fluid>
        <div>
          <Row style={{ backgroundColor: "#F4F0E3" }}>
            <div className="offset-xl-1 col-xl-5">
              <div className=" ml-5 mt-5">
                <div>
                  <h1 className="heading">Your Profile</h1>
                  <div>
                    <ul className="profile ">
                      <li className="d-inline-block px-2 mx-2 ">
                        <Link
                          className="link text-decoration-none"
                          onClick={() => setShow(true)}
                        >
                          Logout
                        </Link>
                        <Modal
                          show={show}
                          onHide={() => setShow(false)}
                          dialogClassName="customModal2"
                          aria-labelledby="example-custom-modal-styling-title"
                        >
                          <Modal.Header className="colorModal">
                            <Button
                              className="closeButton border-0 shadow-none rounded-0"
                              onClick={handleClose}
                            >
                              <img
                                className="m-xl-3 m-lg-3 m-md-3 m-1"
                                src={crossIcon}
                              />

                              {/* <img className="mx-1 img-fluid" src={crossIcon} /> */}
                            </Button>
                            <Modal.Title className="colorModal"></Modal.Title>
                          </Modal.Header>
                          <Modal.Body className="colorModal">
                            <LogoutComponent />
                          </Modal.Body>
                        </Modal>
                      </li>
                      <li className=" d-inline-block mx-2">
                        <Link className="link text-decoration-none">
                          Password Rest
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <Col xl={6}>
              <img src={Group} class="img-fluid" />
            </Col>
          </Row>
        </div>
        {/* //................................................................// */}

        <Row>
          <div className=" col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
            <Row>
              <div
                className=" col-xl-11  col-lg-11 col-md-8 px-5"
                style={{
                  backgroundColor: "#FCF9F2",
                  position: "relative",
                  marginTop: "-14%",
                }}
              >
                {/* <div
                  class="card"
                  style={{ backgroundColor: "#FCF9F2", height: "20rem" }}
                >
                  <div class="card-body mt-4 mx-5 ">
                    <h1 class="card-title ">Get Verified</h1>
                    <h6 class="card-subtitle mb-2 text-danger display-6">
                      Status: Pending
                    </h6>
                    <p class="card-text lead">
                      Verified account status allows you to redeem gift cards
                      and earn coins. Verify the below to confirm your account.
                    </p>
                   
                  </div>
                </div> */}

                <div className="mt-5 pt-5 pb-3 px-5">
                  <h1>Get Verified</h1>
                  <p style={{ color: "#F47458" }}>Status: Pending</p>
                  <p className="text-muted">
                    Verified account status allows you to redeem gift cards and
                    earn coins. Verify the below to confirm your account.
                  </p>
                </div>
                <div
                  className="pb-5 mb-5"
                  style={{ backgroundColor: "#FCF9F2" }}
                >
                  <ul className="footerbtn">
                    <li>
                      <div
                        style={{
                          backgroundColor: "#FA7269",
                          borderRadius: "5px",
                        }}
                        className="mr-5 py-2 mb-3"
                      >
                        <span
                          className="ml-1 py-1 px-2"
                          style={{ backgroundColor: "#FCF9F2" }}
                        >
                          <img src={TimeCardRed} />
                        </span>
                        <span className="pl-3" style={{ color: "white" }}>
                          About You (Status: Pending)
                        </span>
                      </div>
                    </li>
                    <li>
                      <div
                        style={{
                          backgroundColor: "#50BFA5",
                          borderRadius: "5px",
                        }}
                        className="mr-5 py-2"
                      >
                        <span
                          className="ml-1 py-1 px-2"
                          style={{ backgroundColor: "#FCF9F2" }}
                        >
                          <img src={TimeCardGreen} />
                        </span>
                        <span className="pl-3" style={{ color: "white" }}>
                          Your Email (Status: Verified)
                        </span>
                      </div>
                    </li>
                    <li>
                      <div
                        style={{
                          backgroundColor: "#FA7269",
                          borderRadius: "5px",
                        }}
                        className="mr-5 py-2 my-3"
                      >
                        <span
                          className="ml-1 py-1 px-2"
                          style={{ backgroundColor: "#FCF9F2" }}
                        >
                          <img src={TimeCardRed} />
                        </span>
                        <span className="pl-3" style={{ color: "white" }}>
                          Phone number (Status: Pending)
                        </span>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </Row>

            <Row className="  my-5  ">
              <div className=" col-xl-11  col-lg-11 col-md-11 col-sm-11 col-xs-11  box2">
                <div
                  style={{ backgroundColor: "#FCF9F2" }}
                  className="mt-5 pt-5 pb-5 px-5"
                >
                  <h1>Account Password</h1>
                  <p>You can reset your password below.</p>
                  <ProfileComponentPasswordForm
                    user={{
                      oldPassword: "",
                      NewPassword: "",
                    }}
                  />
                </div>

                {/* <div
                  class="card"
                  style={{ backgroundColor: "#FCF9F2", height: "28rem" }}
                >
                  <div className="offset-md-1 col-md-9 col-sm-9 mt-5">
                    <div class="card-body">
                      <h1 class="card-title">Account Password</h1>
                      <p class="card-text">
                        You can reset your password below.
                      </p>
                      <ProfileComponentPasswordForm
                        user={{
                          oldPassword: "",
                          NewPassword: "",
                        }}
                      /> */}
                {/* <Form class="card-text">
                        <Form.Group className="mb-3 email">
                          <Form.Control
                            className=" form2"
                            type="text"
                            placeholder="Old Password"
                          />
                        </Form.Group>

                        <Form.Group
                          className="mb-3"
                          controlId="formBasicPassword"
                        >
                          <Form.Control
                            className=" form2"
                            type="text"
                            placeholder="New Password"
                          />
                        </Form.Group>
                        <Col></Col>
                        <a href="#" class="btn btn border-0 button2  px-4">
                          Change Password
                        </a>
                      </Form> */}
                {/* </div>
                  </div>
                </div> */}
              </div>
            </Row>

            <Row>
              <div className=" col-xl-11 col-lg-11 col-md-11">
                <div
                  className="mt-5 pt-5 pb-5 px-5"
                  style={{ backgroundColor: "#FCF9F2" }}
                >
                  <h1>Delete Account</h1>
                  <p>
                    To delete your account click the button below, this will
                    also delete any coins you have earned.
                  </p>
                  <Button className="border-0 customButton">
                    <b>Delete Account</b>
                  </Button>
                </div>
                {/*
                  <Card style={{ height: "18rem", backgroundColor: "#FCF9F2" }}> 
                  <Card.Body className=" mt-4 mx-5">
                    <Card.Title>
                      <h1>Delete Account</h1>
                    </Card.Title>
                    <Card.Text className=" text-justify ">
                      To delete your account click the button below, this will
                      also delete any coins you have earned.
                    </Card.Text>
                    <Button className="border-0 button2">
                      <b>Delete Account</b>
                    </Button>
                  </Card.Body>
                </Card> */}
              </div>
            </Row>
          </div>
          {/* //........................................................................// */}
          {/* <div className=" col-md-1 my-3">
            <Row>
              <img src={Rectangle1} />
            </Row>
            <Row className=" my-5">
              <img src={Rectangle2} />
            </Row>
            <Row className=" my-5">
              <img src={Rectangle3} />
            </Row>
          </div> */}
          {/* //........................................................................// */}

          <div className="  col-xl-8  col-lg-7 col-md-10 col-sm-12 col-xm-12 ">
            <div className="line1">
              {/* <img src={Rectangle1} className="position-absolute mt-5" /> */}
              <div className="position-relative ml-3  ml-md-5 ml-sm-4  ml-xm-3  ">
                <Row className=" my-5 ">
                  <div className=" col-xl-11 col-lg-11  col-md-8  col-sm-7  ">
                    <div
                      className="d-flex"
                      style={{ justifyContent: "space-between" }}
                    >
                      <div style={{ width: "75%" }}>
                        <h1>About You</h1>
                        <p>Please fill out your profile below.</p>
                      </div>
                      <div>
                        <img className=" my-3" src={Timer} />
                      </div>
                    </div>
                    {/* <h1>About You</h1>
                    <p>Please fill out your profile below.</p> */}
                  </div>
                  {/* <div className=" col-xl-2 col-lg-2 col-md-2  col-sm-2  col-xs-2  ">
                    <img className=" my-3" src={Timer} />
                  </div> */}
                </Row>
                <Row>
                  <Col xl={10} lg={10} md={12} sm={12} xs={12}>
                    <ProfileComponentAboutForm
                      user={{
                        fullName: "",
                        dateOfBirth: "",
                        gender: "",
                        country: "",
                        state: "",
                        city: "",
                        postalCode: "",
                      }}
                    />
                  </Col>
                </Row>
              </div>
            </div>

            <div className="line2">
              <div className="position-relative ml-3  ml-md-5 ml-sm-4  ml-xm-3 ">
                <Row className=" mt-5 ">
                  <div className=" col-xl-11 col-lg-11  col-md-11  col-sm-8   col-sx-7 mt-5  ">
                    <div
                      className="d-flex"
                      style={{ justifyContent: "space-between" }}
                    >
                      <div style={{ width: "75%" }}>
                        <h1>Your email</h1>
                        <p>
                          Please confirm your email by clicking the link sent
                          during sign up. If you didn’t receive an email,&nbsp;
                          <Link className="p1 text-decoration-none">
                            click here
                          </Link>
                          &nbsp;to have a new one sent.
                        </p>
                      </div>
                      <div>
                        <img className="mt-5 " src={Tick} />
                      </div>
                    </div>
                  </div>
                </Row>

                {/* <div className="mt-5 ">
              <img src={Rectangle2} className="position-absolute mt-5 " />
              <div className="position-relative ml-4 ">
                <Row className=" mt-5 ">
                  <div className=" col-xl-7 col-lg-7  col-md-7  col-sm-7   col-sx-7 mt-5  ">
                    <h1>Your email</h1>
                    <p>
                      Please confirm your email by clicking the link sent during
                      sign up. If you didn’t receive an email,&nbsp;
                      <Link className="p1 text-decoration-none">
                        click here
                      </Link>
                      &nbsp;to have a new one sent.
                    </p>
                  </div>
                  <div className="col-xl-4 col-lg-4  col-md-4  col-sm-4   col-sx-4 mt-5 ">
                    <img className="float-right my-3" src={Tick} />
                  </div>
                </Row> */}

                <Row>
                  <Col className="xl={6} lg={10} md={12} sm={12} xs={12}">
                    <ProfileComponentEmailForm
                      user={{
                        email: "",
                      }}
                    />
                  </Col>
                </Row>
              </div>
            </div>

            <div className="line3">
              <div className="position-relative ml-3  ml-md-5 ml-sm-4  ml-xm-3 ">
                <Row className=" mt-5 ">
                  <div className="col-xl-11 col-lg-11  col-md-11  col-sm-11   col-sx-11  mt-5  ">
                    <div
                      className="d-flex"
                      style={{ justifyContent: "space-between" }}
                    >
                      <div style={{ width: "75%" }}>
                        <h1>Your phone number</h1>
                        <p>
                          Please confirm your phone number by entering the code
                          sent to you, If you didn’t receive a code,&nbsp;
                          <Link className="p1 text-decoration-none">
                            click here
                          </Link>
                          &nbsp;to have a new one sent.
                        </p>
                      </div>
                      <div>
                        <img
                          className="float-right pb-5 img-fluid"
                          src={Timer}
                        />
                      </div>
                    </div>
                  </div>
                </Row>
                <Row>
                  <Col xl={6} lg={10} md={12} sm={12} xs={12}>
                    <ProfileComponentPhoneNumberForm
                      user={{
                        phoneNumber: "",
                      }}
                    />
                  </Col>
                </Row>
              </div>
            </div>
          </div>
        </Row>
      </Container>
      <DashboardFooter />
    </>
  );
};

export default ProfileComponent;
