import "../../../Validations/formik.css";
import React, { useState } from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import { withFormik } from "formik";
import { TextInput } from "../../../Validations/TextField";
import * as Yup from "yup";
import { formValidtorProfileAbout } from "../../../Validations/ProfileValidation";
import { DatePickerField } from "./../../DatePicker/DatePicker";
import "react-datepicker/dist/react-datepicker.css";
import { SelectGender, SelectState } from "./Dropdowns";
import { countryDatabase } from "../../../Dataset/countries";

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape(formValidtorProfileAbout),
  mapPropsToValues: ({ user }) => ({
    ...user,
  }),
  handleSubmit: (payload, { setSubmitting }) => {
    alert(JSON.stringify(payload));
    setSubmitting(true);
  },
  displayName: "MyForm",
});

const MyForm = (props) => {
  const {
    values,
    touched,
    errors,
    dirty,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    isSubmitting,
  } = props;

  return (
    <Form onSubmit={handleSubmit}>
      <Row>
        <Col xl={6} lg={12} md={12} sm={12} xs={12}>
          <TextInput
            id="fullName"
            type="text"
            placeholder="Full Name"
            error={touched.fullName && errors.fullName}
            value={values.fullName}
            onChange={handleChange}
            onBlur={handleBlur}
            className="formField"
          />
          <DatePickerField
            name="dateOfBirth"
            placeholderText="Date of Birth"
            error={touched.dateOfBirth && errors.dateOfBirth}
            onBlur={handleBlur}
          />

          <SelectGender
            id="gender"
            error={touched.gender && errors.gender}
            value={values.gender}
            onChange={handleChange}
            onBlur={handleBlur}
            className="formField"
          />

          <SelectState
            id="country"
            error={touched.country && errors.country}
            value={values.country}
            onChange={handleChange}
            onBlur={handleBlur}
            data={countryDatabase}
            selected="Country"
            className="formField"
          />
        </Col>
        <Col xl={6} lg={12} md={12} sm={12} xs={12} className="pt-2">
          <SelectState
            id="state"
            error={touched.state && errors.state}
            value={values.state}
            onChange={handleChange}
            onBlur={handleBlur}
            data={countryDatabase}
            selected="State"
            className="formField"
          />
          <SelectState
            id="city"
            error={touched.city && errors.city}
            value={values.city}
            onChange={handleChange}
            onBlur={handleBlur}
            data={countryDatabase}
            selected="City"
            className="formField"
          />

          <TextInput
            id="postalCode"
            type="number"
            placeholder="Postal Code"
            error={touched.postalCode && errors.postalCode}
            value={values.postalCode}
            onChange={handleChange}
            onBlur={handleBlur}
            className="formField"
          />
          <Button
            type="submit"
            disabled={isSubmitting}
            variant="warning"
            className="border-0 customButton px-4"
          >
            <b>Save</b>
          </Button>
          {/* <TextInput
        id="dateOfBirth"
        type="date"
        placeholder="Date of Birth"
        error={touched.dateOfBirth && errors.dateOfBirth}
        value={values.dateOfBirth}
        onChange={handleChange}
        onBlur={handleBlur}
        className="form"
      /> */}
          {/* <Button
        type="button"
        className="outline"
        onClick={handleReset}
        disabled={!dirty || isSubmitting}
      >
        Reset
      </Button> */}
        </Col>
      </Row>
    </Form>
  );
};
export const ProfileComponentAboutForm = formikEnhancer(MyForm);
