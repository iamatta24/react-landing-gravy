import React, { useState } from "react";
import { Link } from "react-router-dom";
import HeaderComponent from "../UserHeaderFooterComponnet/HeaderComponent";
import FooterComponent from "../UserHeaderFooterComponnet/FooterComponent";
import { Row, Col, Container, Button, Form } from "react-bootstrap";
import Coin from "../../../Asserts/Coin.png";
import "./SignUp.css";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import SignUpFormValid from "./SignUpFrom";
import { postUser } from "./../../Redux/actions/Signup/userAction";
import "./SignUp.css";
// const initialValues = {
//   email: "",
// };

const SignUpComponent = ({ postUser }) => {
  const history = useHistory();

  const handleButton2 = () => {
    const path = `newPath`;
    history.push("/signIn");
  };

  // const [user, setUser] = useState({
  //   email: "",
  //   password: "",
  //   re_typePassword: "",
  // });

  // const handleChange = (e) => {
  //   setUser({ ...user, [e.target.name]: e.target.value });
  // };

  // const handleSubmit = (e) => {
  //   e.preventDefault();
  //   postUser(user);
  // };
  return (
    <>
      <div className=" pt-4" style={{ backgroundColor: "#E5E5E5" }}>
        <HeaderComponent />
      </div>

      <Container fluid style={{ backgroundColor: " #E5E5E5" }}>
        <Row className=" pt-5  pb-5 " style={{ backgroundColor: " #E5E5E5" }}>
          <div
            className="pt-3 px-5 py-0 pb-3 offset-xl-2 col-xl-4  offset-lg-1 col-lg-5  offset-md-1 col-md-5 offset-sm-2 col-sm-8 offset-xs-1 col-xs-11"
            style={{ backgroundColor: "#FFFFFF" }}
          >
            <div className="p-xl-4 mx-xl-4">
              <h1 className="mt-5 mb-4 formText">Sign Up</h1>

              <SignUpFormValid
                user={{
                  email: "",
                  password: "",
                  passwordConfirm: "",
                  checkBox: "",
                }}
              />

              <p className="termsCondition my-5 py-5">
                Already a Member? &nbsp;
                <Link to="/signIn">
                  <a className="forgotpassword">Sign In</a>
                </Link>
              </p>
            </div>
          </div>

          <Col xl={5} lg={5} md={5} sm={8} xs={12} className="py-4">
            <Row
              className="px-xl-5 pb-xl-5 py-xl-5 my-xl-5 mr-xl-5 pr-xl-5  pl-lg-5  pt-lg-1 mb-lg-5   pt-md-1 mb-md-5 "
              style={{ backgroundColor: " #FCF9F2" }}
            >
              <div
                className="px-xl-5 py-xl-5  offset-xl-0 col-xl-12 offset-lg-0 col-lg-9  offset-md-1 col-md-10 offset-sm-0 col-sm-10 offset-xs-0 col-xs-11"
                style={{ backgroundColor: " #FCF9F2" }}
              >
                <p className="para1">Be a part of the data revolution </p>
                <h1 className="giftcard">
                  Join today and earn a $50 Gift card{" "}
                </h1>
                <p className="para2">
                  Simply install the Gravy extension to your favourite browser
                  and start earning for using the web as you usually would.
                </p>
                <p className="para2"></p>
                <div className="buttonWrapper text-center	">
                  <Button
                    className="px-5 py-2 mb-5 border-0 customButton"
                    type="button"
                    onClick={handleButton2}
                  >
                    <b>Sign In</b>
                  </Button>
                </div>
                <img src={Coin} class="img-fluid fix-img" />
              </div>
            </Row>
          </Col>
        </Row>
        <FooterComponent />
      </Container>
    </>
  );
};

export default connect(null, { postUser })(SignUpComponent);
//export default SignUpComponent;
