import "../../../Validations/formik.css";
import React from "react";
import { Button, Form } from "react-bootstrap";
import { withFormik } from "formik";
import { TextInput } from "../../../Validations/TextField";
import * as Yup from "yup";
import { formValidtorSignUp } from "./../../../Validations/SignUpValidation";
import { postUser } from "../../Redux/actions/Signup/userAction";
import { connect } from "react-redux";
import "./SignUp.css";

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape(formValidtorSignUp),
  mapPropsToValues: ({ user }) => ({
    ...user,
  }),
  handleSubmit: (payload, { props, setSubmitting }) => {
    alert(JSON.stringify(payload));
    setSubmitting(false);
    props.postUser(payload);
  },
  displayName: "MyForm",
});

const MyForm = (props) => {
  const {
    values,
    touched,
    errors,
    dirty,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    isSubmitting,
  } = props;

  return (
    <Form onSubmit={handleSubmit}>
      <TextInput
        id="email"
        type="email"
        placeholder="Email"
        error={touched.email && errors.email}
        value={values.email}
        onChange={handleChange}
        onBlur={handleBlur}
        className="formField mb-4"
      />
      <TextInput
        id="password"
        type="password"
        placeholder="Password"
        error={touched.password && errors.password}
        value={values.password}
        onChange={handleChange}
        onBlur={handleBlur}
        className="formField mb-4"
      />
      <TextInput
        id="passwordConfirm"
        type="password"
        placeholder="Re-type password"
        error={touched.passwordConfirm && errors.passwordConfirm}
        value={values.passwordConfirm}
        onChange={handleChange}
        onBlur={handleBlur}
        className="formField mb-5"
      />
      {/* <div className="d-flex"> */}
      <TextInput
        id="checkBox"
        type="checkbox"
        placeholder="Re-type password"
        error={touched.checkBox && errors.checkBox}
        value={values.checkBox}
        onChange={handleChange}
        onBlur={handleBlur}
        className="mt-4"
      />
      {/* <span className="termsCondition mb-5">
          I agree to the &nbsp;
          <a className="pr-5 termsCondition2" href="#">
            terms and conditions
          </a>
        </span> */}
      {/* </div> */}
      {/* {touched.checkBox && errors.checkBox && (
        <div
          className="input-feedback"
          style={{ color: "red", marginBottom: "15px", marginTop: "-20px" }}
        >
          {" "}
          {errors.checkBox}{" "}
        </div>
      )
      } */}

      {/* <Button
        type="button"
        className="outline"
        onClick={handleReset}
        disabled={!dirty || isSubmitting}
      >
        Reset
      </Button> */}
      <Button
        type="submit"
        disabled={isSubmitting}
        className="border-0 customButton px-4 py-2"
      >
        <b>Sign Up</b>
      </Button>
    </Form>
  );
};
const SignUpFormValid = formikEnhancer(MyForm);
export default connect(null, { postUser })(SignUpFormValid);
