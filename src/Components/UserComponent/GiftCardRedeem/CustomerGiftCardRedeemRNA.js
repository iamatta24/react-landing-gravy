import React from "react";
import { Row, Container } from "react-bootstrap";
import HeaderComponent from "../UserDashboardComponent/DashboardHeaderFooter/HeaderComponent";
import FooterComponent from "./../UserDashboardComponent/DashboardHeaderFooter/FooterComponent";
import RedeemGiftCardComponent from "./RedeemGiftCardComponent";
import KeepBrowsingComponent from "./KeepBrowsingComponent";
import CongratulationsComponent from "./CongratulationsComponent";

const customerGiftCardRedeemRNA = () => {
  return (
    <>
      <HeaderComponent />
      <Container fluid className="font">
        <RedeemGiftCardComponent />

        {/* <KeepBrowsingComponent /> */}

        <CongratulationsComponent />

        <Row>
          <FooterComponent />
        </Row>
      </Container>
    </>
  );
};

export default customerGiftCardRedeemRNA;
