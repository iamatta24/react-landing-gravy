import "../../../Validations/formik.css";
import React from "react";
import { Button, Form } from "react-bootstrap";
import { withFormik } from "formik";
import { TextInput } from "../../../Validations/TextField";
import { formValidtor } from "../../../Validations/GiftCardRedeemValidation";
import { postGift } from "./../../Redux/actions/GiftCardRedeem/GiftAction";
import { connect } from "react-redux";

import * as Yup from "yup";

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape(formValidtor),
  mapPropsToValues: ({ user }) => ({
    ...user,
  }),
  handleSubmit: (payload, { props, setSubmitting }) => {
    alert(JSON.stringify(payload));
    setSubmitting(true);
    props.postGift(payload);
  },
  displayName: "MyForm",
});
const MyForm = (props) => {
  const {
    values,
    touched,
    errors,
    dirty,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    isSubmitting,
  } = props;
  return (
    <Form onSubmit={handleSubmit}>
      <TextInput
        id="giftCardName"
        type="text"
        placeholder="Gift card name"
        error={touched.giftCardName && errors.giftCardName}
        value={values.giftCardName}
        onChange={handleChange}
        onBlur={handleBlur}
        className="form3  "
      />
      <TextInput
        id="giftCardAmount"
        type="number"
        placeholder="Gift card amount"
        error={touched.giftCardAmount && errors.giftCardAmount}
        value={values.giftCardAmount}
        onChange={handleChange}
        onBlur={handleBlur}
        className="form3 "
      />
      {/* <TextInput
        id="email"
        type="email"
        label="Email"
        placeholder="Enter your email"
        error={touched.email && errors.email}
        value={values.email}
        onChange={handleChange}
        onBlur={handleBlur}
      /> */}
      {/* <Button
        type="button"
        className="outline"
        onClick={handleReset}
        disabled={!dirty || isSubmitting}
      >
        Reset
      </Button> */}
      <Button
        type="submit"
        disabled={isSubmitting}
        variant="warning"
        className="border-0 font-weight-bold customButton my-2 px-5 shadow-none"
      >
        Submit
      </Button>
    </Form>
  );
};
const MyEnhancedForm = formikEnhancer(MyForm);

export default connect(null, { postGift })(MyEnhancedForm);
