import React from "react";
import { Row, Col } from "react-bootstrap";

import StarbucksGirl from "../../../Asserts/StarbucksGirl.png";
import "./Gift.css";
import PendingSuccess from "./../../Common/PendingSuccess";

const redeemGiftCardComponent = () => {
  return (
    <>
      <Row className="header1 pt-5">
        <Col xl={4} lg={12} md={12} sm={12}>
          <img src={StarbucksGirl} className="img-fluid mt-5 pt-3" />
        </Col>
        <Col xl={8} lg={12} md={12} sm={12}>
          <Row className="mt-5 mx-5">
            <Col xl={12} className="mt-4 ">
              <h1 className="heading2">Redeem your gift card</h1>
              <h5 className="heading4">
                For more information about gift card redeems and requirements
                per country,&nbsp;
                <a href="#" className="p1 text-decoration-none">
                  click here
                </a>
                .
              </h5>
            </Col>
          </Row>
          <Row className="mt-5 mx-xl-5 mx-lg-5 mx-md-5 coinsDown">
            {/* <Col
              xl={1}
              style={{ backgroundColor: "#FCF9F2" }}
              className="py-5 px-4"
            >
              <img src={Check} className="img-fluid" />
            </Col> */}
            <Col
              xl={6}
              lg={6}
              md={12}
              sm={12}
              xs={12}
              style={{ backgroundColor: "#FCF9F2", padding: "0px " }}
            >
              {/* <img src={Check} className="img-fluid pr-5 " /> */}
              <div className="pt-5 d-flex imgSuccess">
                <PendingSuccess
                  type="sucess"
                  size="50px"
                  className="img-fluid mx-4"
                />

                <h1 className="cointext">
                  Total coins: <b>0</b>
                </h1>
              </div>
            </Col>

            {/* <Col xl={1} style={{ backgroundColor: "#FCF9F2" }} className="py-5">
              <img src={Time2} className="img-fluid" />
            </Col> */}
            <Col
              xl={6}
              lg={6}
              md={12}
              sm={12}
              xs={12}
              style={{ backgroundColor: "#FCF9F2" }}
              className="py-5 d-flex"
            >
              {/* <img src={Time2} className="img-fluid pr-5" /> */}
              <PendingSuccess
                type="pending"
                size="50px"
                className="img-fluid mr-4 imgPending"
              />
              <h1 className="cointext">
                Coins required: <b>0</b>
              </h1>
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};

export default redeemGiftCardComponent;
