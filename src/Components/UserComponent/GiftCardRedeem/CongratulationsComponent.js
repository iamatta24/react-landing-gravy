import React from "react";
import { Row, Col } from "react-bootstrap";
import MyEnhancedForm from "./GiftCardRedeemForm";

const congratulationsComponents = () => {
  return (
    <>
      <Row className="text-center pt-5 mt-5">
        <Col>
          <h1 className="heading1">
            Congratulations, your gift card is ready!
          </h1>
        </Col>
      </Row>
      <Row className="text-center py-3">
        <Col
          xl={4}
          lg={6}
          md={8}
          sm={12}
          xs={12}
          className="text-center offset-xl-4 offset-lg-3 offset-md-2"
        >
          <div>
            <p className="paragraph">
              Fill in the form below to redeem your gift card. Each submission
              will remove 100 coins from your balance.
            </p>
          </div>
        </Col>
      </Row>
      <Row>
        <Col
          xl={4}
          xl={{ offset: 4 }}
          lg={4}
          lg={{ offset: 4 }}
          md={6}
          md={{ offset: 3 }}
          sm={8}
          sm={{ offset: 2 }}
          xs={10}
          xs={{ offset: 1 }}
          className="col-10 eForm"
        >
          <MyEnhancedForm user={{ giftCardName: "", giftCardAmount: "" }} />
        </Col>
        {/* <Col xl={4} lg={4}></Col> */}
      </Row>
    </>
  );
};

export default congratulationsComponents;

//export default connect(null, { postGift })(congratulationsComponents);
