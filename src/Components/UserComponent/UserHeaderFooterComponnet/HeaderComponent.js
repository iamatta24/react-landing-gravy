import React from "react";
import {
  Row,
  Col,
  Container,
  Button,
  Nav,
  NavDropdown,
  Navbar,
  Form,
  FormControl,
} from "react-bootstrap";
import "./HeaderFooter.css";

import GravyIcon from "../../../Asserts/GravyIcon.png";

const headerComponent = () => {
  return (
    <>
      <Container fluid style={{ backgroundColor: " #E5E5E5" }}>
        <Navbar bg="" expand="lg">
          <Navbar.Brand href="#">
            {" "}
            <img src={GravyIcon} />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mx-auto"></Nav>

            <Nav.Link className="text  mx-2" href="/profileComponent ">
              About
            </Nav.Link>
            <Nav.Link className="text  mx-2" href="#action2">
              Help
            </Nav.Link>
            <Nav.Link className="text  mx-2" href="#action2">
              Careers
            </Nav.Link>
            <Nav.Link className="text  mx-2 mr-3" href="#action2">
              News
            </Nav.Link>

            <Button className="button border-0 shadow-none px-4  py-2 mx-5 ml-1">
              Install Extension
            </Button>
          </Navbar.Collapse>
        </Navbar>
      </Container>

      {/* <Container fluid={true} style={{marginTop: "40px"}}>
        <Row>
          
          <Col xl={7}md={7} sm={7} className = "mx-5" style={{backgroundColor: ""}} >
            <img src={GravyIcon} />
          </Col>

          <Col  xl={4} md={4} sm={4}  >
            <ul className="header">
              <li className="text"> About</li>
              <li className="text"> Help</li>
              <li className="text"> Careers</li>
              <li className="text"> News</li>
              <Button className="button">Install Extension</Button>
            </ul>
           
          </Col>
        </Row>
      </Container> */}
    </>
  );
};

export default headerComponent;
