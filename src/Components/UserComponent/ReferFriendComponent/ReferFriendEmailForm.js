import "../../../Validations/formik.css";
import React from "react";
import { Button, Form } from "react-bootstrap";
import { withFormik } from "formik";
import { TextInput } from "../../../Validations/TextField";
import { formValidtorEmail } from "../../../Validations/ReferFriendValidation";
import * as Yup from "yup";

import "./ReferFriend.css";
import { postRefer } from "./../../Redux/actions/ReferFriend/ReferAction";
import { connect } from "react-redux";

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape(formValidtorEmail),
  mapPropsToValues: ({ user }) => ({
    ...user,
  }),
  handleSubmit: (payload, { props, setSubmitting }) => {
    alert(JSON.stringify(payload));
    setSubmitting(false);
    props.postRefer(payload);
  },
  displayName: "MyForm",
});
const MyForm = (props) => {
  const {
    values,
    touched,
    errors,
    dirty,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    isSubmitting,
  } = props;
  return (
    <Form onSubmit={handleSubmit} className="card-text">
      <TextInput
        id="inviteByEmail"
        type="email"
        placeholder="Friends Email Address"
        error={touched.inviteByEmail && errors.inviteByEmail}
        value={values.inviteByEmail}
        onChange={handleChange}
        onBlur={handleBlur}
        className="form3"
      />
      {/* <TextInput
        id="giftCardAmount"
        type="text"
        placeholder="Gift card amount"
        error={touched.giftCardAmount && errors.giftCardAmount}
        value={values.giftCardAmount}
        onChange={handleChange}
        onBlur={handleBlur}
      /> */}
      {/* <TextInput
        id="email"
        type="email"
        label="Email"
        placeholder="Enter your email"
        error={touched.email && errors.email}
        value={values.email}
        onChange={handleChange}
        onBlur={handleBlur}
      /> */}
      {/* <Button
        type="button"
        className="outline"
        onClick={handleReset}
        disabled={!dirty || isSubmitting}
      >
        Reset
      </Button> */}
      <Button
        type="submit"
        disabled={isSubmitting}
        variant="warning"
        className="border-0 button2 my-3 px-5 shadow-none"
      >
        Invite
      </Button>
    </Form>
  );
};
const ReferFriendEmailForm = formikEnhancer(MyForm);

export default connect(null, { postRefer })(ReferFriendEmailForm);
