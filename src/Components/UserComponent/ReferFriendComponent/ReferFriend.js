import { React, useState } from "react";
import { Row, Container, Button, Form } from "react-bootstrap";

import DashboardHeader from "../UserDashboardComponent/DashboardHeaderFooter/HeaderComponent";
import DashboardFooter from "../UserDashboardComponent/DashboardHeaderFooter/FooterComponent";
import Refer from "../../../Asserts/Refer.png";
import Facebook from "../../../Asserts/Facebook.svg";
import Twitter from "../../../Asserts/Twitter.svg";
import Gmail from "../../../Asserts/Gmail.svg";
import ReferFriendEmailForm from "./ReferFriendEmailForm";

import {
  FacebookShareButton,
  TwitterShareButton,
  EmailShareButton,
} from "react-share";

import "./ReferFriend.css";

const ReferFriend = () => {
  const [link, setLink] = useState({
    url: "www.joingravy.com/friend/10297873",
  });

  const handleChange = (e) => {
    setLink({ ...link, [e.target.name]: [e.target.value] });
  };

  return (
    <>
      <DashboardHeader />
      <Container fluid>
        <div>
          <Row style={{ backgroundColor: "#F4F0E3" }}>
            <div className="col-xl-6 col-lg-6 col-md-6 mt-xl-5 pt-xl-5 mt-lg-5 pt-lg-5 mt-md-5 pt-md-4 mt-sm-5 pt-sm-4">
              <img src={Refer} class="img-fluid" />
            </div>
            <div className="my-3 col-xl-4  col-lg-5  col-md-6 mx-xl-5 mx-lg-4 mt-xl-5 pt-xl-5 mt-lg-5 pt-lg-5 mt-md-2 pt-md-2">
              <h1>Earn a $20 gift card for you and a friend</h1>
              <p>
                Earn 100 coins for each friend you invite, and we’ll also give
                100 coins to your friend for joining.
              </p>
            </div>
          </Row>
          <Row>
            <div className="offset-xl-1 col-xl-5 offset-lg-1 col-lg-5  col-md-6   col-sm-11  col-xs-11">
              <div class="card cardCol">
                <div
                  class="card-header py-5 text-center"
                  style={{ backgroundColor: "#DDEDE0", height: "8rem" }}
                >
                  <h2>Invite by Email</h2>
                </div>
                <div
                  class="card-body py-5 px-xl-5 px-lg-5 px-md-5 px-3"
                  style={{ backgroundColor: "#FCF9F2", height: "20rem" }}
                >
                  <ReferFriendEmailForm user={{ inviteByEmail: "" }} />
                </div>
              </div>
            </div>

            <div className="col-xl-5 col-lg-5  col-md-6  col-sm-11  col-xs-11">
              <div class="card cardCol">
                <div
                  class="card-header py-5 text-center"
                  style={{ backgroundColor: "#DDEDE0", height: "8rem" }}
                >
                  <h2>Share your link</h2>
                </div>
                <div
                  class="card-body py-5 px-xl-5 px-lg-5 px-md-5 px-3 "
                  style={{ backgroundColor: "#FCF9F2", height: "20rem" }}
                >
                  <Form class="card-text pt-5">
                    <Form.Group className="mb-3 email text-center">
                      <Form.Control
                        id="link"
                        name="url"
                        className="form3"
                        value={link.url}
                        type="text"
                        //placeholder="www.joingravy.com/friend/10297873"
                        onChange={handleChange}
                        disabled
                      />
                    </Form.Group>
                    <Button
                      variant="warning"
                      className="border-0 button2 my-3 px-5 shadow-none"
                      onClick={() => {
                        navigator.clipboard.writeText(link.url);
                      }}
                    >
                      Copy
                    </Button>

                    <Row>
                      <div className="my-4 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <FacebookShareButton url={"https://www.spyresync.com/"}>
                          <img className="mx-1" src={Facebook} roundedCircle />
                        </FacebookShareButton>

                        <TwitterShareButton url={"https://www.spyresync.com/"}>
                          <img className="mx-1" src={Twitter} roundedCircle />
                        </TwitterShareButton>

                        <EmailShareButton url={"https://www.spyresync.com/"}>
                          <img className="mx-1" src={Gmail} roundedCircle />
                        </EmailShareButton>
                      </div>
                    </Row>
                  </Form>
                </div>
              </div>
            </div>
          </Row>
        </div>
      </Container>
      <DashboardFooter />
    </>
  );
};

export default ReferFriend;
