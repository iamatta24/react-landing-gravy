import React from "react";
import { useHistory } from "react-router-dom";
import HeaderComponent from "../UserHeaderFooterComponnet/HeaderComponent";
import FooterComponent from "../UserHeaderFooterComponnet/FooterComponent";
import { Row, Col, Container, Button, Form } from "react-bootstrap";
import Coin from "../../../Asserts/Coin.png";
import SignInFormValid from "./SignInFrom";
import "./SignIn.css";

const SignIn = () => {
  const history = useHistory();

  const handleBuuton = () => {
    const path = `newPath`;
    history.push("/");
  };

  return (
    <>
      <div className=" pt-4" style={{ backgroundColor: "#E5E5E5" }}>
        <HeaderComponent />
      </div>

      {/* <Container fluid></Container> */}
      <Container fluid style={{ backgroundColor: "#E5E5E5" }}>
        <Row className=" pt-5  pb-5 " style={{ backgroundColor: "#E5E5E5" }}>
          <div
            className="mb-5 px-5 py-5 my-2 offset-xl-2 col-xl-4  offset-lg-1 col-lg-5  offset-md-1 col-md-5 offset-sm-2 col-sm-8 offset-xs-1 col-xs-11"
            style={{ backgroundColor: "white" }}
          >
            <div className="p-xl-5 mx-xl-4">
              <h1 className="mt-5 mb-5 formText">Sign In</h1>

              <SignInFormValid
                user={{
                  email: "",
                  password: "",
                }}
              />

              <a className="forgotpassword pt-5" href="#">
                forget password?
              </a>
            </div>
          </div>

          {/* <div className="my-5 col-xl-5  col-lg-5  offset-md-2 col-md-9  col-sm-12  col-xs-12"> */}
          <Col xl={5} lg={5} md={5} sm={8} xs={12} className="pt-5 pb-4">
            <Row
              className="px-xl-5   my-xl-5  mr-xl-5 pr-xl-5   pl-lg-5  pt-lg-1 mb-lg-5   pt-md-1 mb-md-5 "
              style={{ backgroundColor: " #FCF9F2" }}
            >
              <div
                className=" px-xl-5  mr-xl-5  offset-xl-0 col-xl-12 offset-lg-0 col-lg-9  offset-md-1 col-md-10 offset-sm-0 col-sm-10 offset-xs-0 col-xs-11"
                style={{ backgroundColor: " #FCF9F2" }}
              >
                <p className="para1">Be a part of the data revolution </p>
                <h1 className="giftcard">
                  Join today and earn a $50 Gift card{" "}
                </h1>
                <p className="para2">
                  Simply install the Gravy extension to your favourite browser
                  and
                </p>
                <p className="para2">
                  start earning for using the web as you usually would.
                </p>
                <div className="buttonWrapper text-center	">
                  <Button
                    className="px-5 py-2  mb-5 border-0  customButton"
                    onClick={handleBuuton}
                  >
                    <b>Sign Up</b>
                  </Button>
                </div>
              </div>
              <img src={Coin} className="img-fluid" />
            </Row>
          </Col>
          {/* </div> */}
        </Row>
        <FooterComponent />
      </Container>
    </>
  );
};

export default SignIn;
