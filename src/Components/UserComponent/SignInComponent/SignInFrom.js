import "../../../Validations/formik.css";
import React from "react";
import { Button, Form } from "react-bootstrap";
import { withFormik } from "formik";
import { TextInput } from "../../../Validations/TextField";
import * as Yup from "yup";
import { formValidtorSignIn } from "../../../Validations/SignInValidation";
import { postSignIn } from "../../Redux/actions/SignIn/SignInAction";
import { connect } from "react-redux";

const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape(formValidtorSignIn),
  mapPropsToValues: ({ user }) => ({
    ...user,
  }),
  handleSubmit: (payload, { props, setSubmitting }) => {
    //alert(JSON.stringify(payload));
    setSubmitting(true);
    props.postSignIn(payload);
  },
  displayName: "MyForm",
});
const MyForm = (props) => {
  const {
    values,
    touched,
    errors,
    dirty,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    isSubmitting,
  } = props;
  return (
    <Form onSubmit={handleSubmit}>
      <TextInput
        id="email"
        type="email"
        placeholder="Email"
        error={touched.email && errors.email}
        value={values.email}
        onChange={handleChange}
        onBlur={handleBlur}
        className="formField mb-4"
      />
      <TextInput
        id="password"
        type="password"
        placeholder="Password"
        error={touched.password && errors.password}
        value={values.password}
        onChange={handleChange}
        onBlur={handleBlur}
        className="formField mb-4"
      />
      {/* <Button
        type="button"
        className="outline"
        onClick={handleReset}
        disabled={!dirty || isSubmitting}
      >
        Reset
      </Button> */}
      <Button
        type="submit"
        disabled={isSubmitting}
        className="border-0 customButton px-4 py-2 my-5 "
      >
        <b>Sign In</b>
      </Button>
    </Form>
  );
};
const SignInFormValid = formikEnhancer(MyForm);
export default connect(null, { postSignIn })(SignInFormValid);
