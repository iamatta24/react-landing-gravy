import React from "react";
import pending from "../../Asserts/Timer.png";
import success from "../../Asserts/Check.png";
import "../UserComponent/GiftCardRedeem/Gift.css";

const PendingSuccess = ({ type, width, height, className }) => {
  switch (type) {
    case "pending":
      return (
        <img
          src={pending}
          alt="pending"
          className={className}
          width={width}
          height={height}
        />
      );
      break;
    case "success":
      return (
        <img
          src={success}
          alt="success"
          className={className}
          width={width}
          height={height}
        />
      );
      break;
    default:
      return (
        <img
          src={success}
          alt="success"
          className={className}
          width={width}
          height={height}
        />
      );
  }
};

export default PendingSuccess;
