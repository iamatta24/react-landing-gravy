import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import signIn from "./Components/UserComponent/SignInComponent/SignIn";
import signUp from "./Components/UserComponent/SignUpComponent/SignUp";
import profileComponent from "./Components/UserComponent/UserProfileComponent/ProfileComponent";
import referFriend from "./Components/UserComponent/ReferFriendComponent/ReferFriend";
import logout from "./Components/UserComponent/UserLogoutComponent/LogoutComponent";
import dashboard from "./Components/UserComponent/UserDashboardComponent/Dashboard/Dashboard";
import customerGiftCardRedeemRNA from "./Components/UserComponent/GiftCardRedeem/CustomerGiftCardRedeemRNA";
import "./App.css";
function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route
            path="/CustomerGiftCardRedeemRNA"
            component={customerGiftCardRedeemRNA}
          />
          <Route path="/referFriend" component={referFriend} />
          <Route path="/profileComponent" component={profileComponent} />
          <Route path="/signIn" component={signIn} />
          <Route path="/logout" component={logout} />
          <Route path="/dashboard" component={dashboard} />
          <Route path="/" component={signUp} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
