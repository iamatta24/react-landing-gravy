import * as Yup from "yup";

export const formValidtor = {
  giftCardName: Yup.string().required("Gift card name is required."),
  giftCardAmount: Yup.number().required("Gift card amount is required."),
};
