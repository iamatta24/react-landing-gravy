import React from "react";
import classnames from "classnames";
import { Form } from "react-bootstrap";
import "../Components/UserComponent/SignUpComponent/SignUp.css";

const InputFeedback = ({ error }) =>
  error ? <div className="input-feedback">{error}</div> : null;

const Label = ({ error, className, children, ...props }) => {
  return (
    <Form.Label className="label" {...props}>
      {children}
    </Form.Label>
  );
};

export const TextInput = ({
  type,
  id,
  label,
  error,
  value,
  onChange,
  className,
  ...props
}) => {
  const classes = classnames({
    "animated shake error": !!error,
  });
  if (type === "checkbox") {
    return (
      <Form.Group className={classes}>
        <div className="d-flex mt-5">
          <Label htmlFor={id} error={error}>
            {label}
          </Label>
          <Form.Check
            id={id}
            className={className}
            type={type}
            value={value}
            onChange={onChange}
            {...props}
            className="customStyle"
          />
          <span className="termsCondition mb-3">
            I agree to the &nbsp;
            <a className="pr-5 termsCondition2" href="#">
              terms and conditions
            </a>
          </span>
        </div>

        <InputFeedback error={error} />
      </Form.Group>
    );
  } else {
    return (
      <Form.Group className={classes}>
        <Label htmlFor={id} error={error}>
          {label}
        </Label>
        <Form.Control
          id={id}
          className={className}
          type={type}
          value={value}
          onChange={onChange}
          {...props}
        />
        <InputFeedback error={error} />
      </Form.Group>
    );
  }
};
