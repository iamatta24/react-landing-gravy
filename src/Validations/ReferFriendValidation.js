import * as Yup from "yup";

export const formValidtorEmail = {
  inviteByEmail: Yup.string()
    .email("Email must be valid")
    .required("Email is required"),
};
