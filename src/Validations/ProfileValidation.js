import * as Yup from "yup";

export const formValidtorProfileAbout = {
  fullName: Yup.string().required("Full name is required"),
  dateOfBirth: Yup.date("Date of birth must be a date").required(
    "Date of birth is required"
  ),
  gender: Yup.string().required("Gender is required"),
  country: Yup.string().required("Country is required"),
  state: Yup.string().required("State is required"),
  city: Yup.string().required("City is required"),
  postalCode: Yup.number().required("Postal code is required"),
};

export const formValidtorProfileEmail = {
  email: Yup.string()
    .email("Email must be valid")
    .required("Email is required"),
};

export const formValidtorProfilePhoneNumber = {
  phoneNumber: Yup.number("Phone number must be valid").required(
    "Phone number is required"
  ),
};

export const formValidtorProfilePassword = {
  oldPassword: Yup.string().required("Old password is required"),
  newPassword: Yup.string()
    .min(6, "Password is too short - should be 6 characters minimum.")
    .required("New password is required")
    .matches(
      /^.*(?=.{8,})(?=.*\d)((?=.*[A-Z]){1}).*$/,
      "Password must contain at least one uppercase character and one number"
    ),
};
