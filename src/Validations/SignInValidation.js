import * as Yup from "yup";

export const formValidtorSignIn = {
  email: Yup.string()
    .email("Email must be valid")
    .required("Email is required"),
  password: Yup.string().required("Password is required"),
};
